#include "mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686.h"
#include <cvodes/cvodes.h>
#include <cvodes/cvodes_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>
#include <udata.h>
#include <math.h>
#include <mex.h>
#include <arInputFunctionsC.h>



 void fu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(void *user_data, double t)
{

  return;
}


 void fsu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(void *user_data, double t)
{

  return;
}


 void fv_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data)
{
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *x_tmp = N_VGetArrayPointer(x);
  data->v[0] = p[0]*x_tmp[0]*x_tmp[1];
  data->v[1] = p[1]*x_tmp[2];
  data->v[2] = (x_tmp[2]+x_tmp[10])*p[2]*x_tmp[3];
  data->v[3] = p[3]*x_tmp[4];
  data->v[4] = p[4]*x_tmp[4]*x_tmp[5];
  data->v[5] = (x_tmp[6]+x_tmp[8])*p[9]*x_tmp[6];
  data->v[6] = p[5]*x_tmp[6]*x_tmp[7];
  data->v[7] = p[6]*x_tmp[8];
  data->v[8] = p[7]*x_tmp[6]*x_tmp[9];
  data->v[9] = p[8]*x_tmp[10];

  return;
}


 void dvdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data)
{
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *x_tmp = N_VGetArrayPointer(x);
  data->dvdx[0] = p[0]*x_tmp[1];
  data->dvdx[10] = p[0]*x_tmp[0];
  data->dvdx[21] = p[1];
  data->dvdx[22] = p[2]*x_tmp[3];
  data->dvdx[32] = (x_tmp[2]+x_tmp[10])*p[2];
  data->dvdx[43] = p[3];
  data->dvdx[44] = p[4]*x_tmp[5];
  data->dvdx[54] = p[4]*x_tmp[4];
  data->dvdx[65] = p[9]*x_tmp[6]+(x_tmp[6]+x_tmp[8])*p[9];
  data->dvdx[66] = p[5]*x_tmp[7];
  data->dvdx[68] = p[7]*x_tmp[9];
  data->dvdx[76] = p[5]*x_tmp[6];
  data->dvdx[85] = p[9]*x_tmp[6];
  data->dvdx[87] = p[6];
  data->dvdx[98] = p[7]*x_tmp[6];
  data->dvdx[102] = p[2]*x_tmp[3];
  data->dvdx[109] = p[8];

  return;
}


 void dvdu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data)
{

  return;
}


 void dvdp_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data)
{
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *x_tmp = N_VGetArrayPointer(x);
  data->dvdp[0] = x_tmp[0]*x_tmp[1];
  data->dvdp[11] = x_tmp[2];
  data->dvdp[22] = (x_tmp[2]+x_tmp[10])*x_tmp[3];
  data->dvdp[33] = x_tmp[4];
  data->dvdp[44] = x_tmp[4]*x_tmp[5];
  data->dvdp[56] = x_tmp[6]*x_tmp[7];
  data->dvdp[67] = x_tmp[8];
  data->dvdp[78] = x_tmp[6]*x_tmp[9];
  data->dvdp[89] = x_tmp[10];
  data->dvdp[95] = (x_tmp[6]+x_tmp[8])*x_tmp[6];

  return;
}


 int fx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, N_Vector xdot, void *user_data)
{
  int is;
  UserData data = (UserData) user_data;
  double *qpositivex = data->qpositivex;
  double *p = data->p;
  double *u = data->u;
  double *v = data->v;
  double *x_tmp = N_VGetArrayPointer(x);
  double *xdot_tmp = N_VGetArrayPointer(xdot);
  fu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(data, t);
  fv_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(t, x, data);
  xdot_tmp[1] = -v[0]+v[1];
  xdot_tmp[2] = v[0]-v[1];
  xdot_tmp[3] = -v[2]+v[3];
  xdot_tmp[4] = v[2]-v[3];
  xdot_tmp[5] = -v[4]+v[5];
  xdot_tmp[6] = v[4]-v[5];
  xdot_tmp[7] = -v[6]+v[7];
  xdot_tmp[8] = v[6]-v[7];
  xdot_tmp[9] = -v[8]+v[9];
  xdot_tmp[10] = v[8]-v[9];
  xdot_tmp[0] = 0.0;
  for (is=0; is<11; is++) {
    if(mxIsNaN(xdot_tmp[is])) xdot_tmp[is] = 0.0;
    if(qpositivex[is]>0.5 && x_tmp[is]<0.0 && xdot_tmp[is]<0.0) xdot_tmp[is] = -xdot_tmp[is];
  }

  return(0);
}


 void fxdouble_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, double *xdot_tmp, void *user_data)
{
  int is;
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *v = data->v;
  fu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(data, t);
  fv_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(t, x, data);
  xdot_tmp[1] = -v[0]+v[1];
  xdot_tmp[2] = v[0]-v[1];
  xdot_tmp[3] = -v[2]+v[3];
  xdot_tmp[4] = v[2]-v[3];
  xdot_tmp[5] = -v[4]+v[5];
  xdot_tmp[6] = v[4]-v[5];
  xdot_tmp[7] = -v[6]+v[7];
  xdot_tmp[8] = v[6]-v[7];
  xdot_tmp[9] = -v[8]+v[9];
  xdot_tmp[10] = v[8]-v[9];
  xdot_tmp[0] = 0.0;
  for (is=0; is<11; is++) {
    if(mxIsNaN(xdot_tmp[is])) xdot_tmp[is] = 0.0;
  }

  return;
}


 void fx0_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(N_Vector x0, void *user_data)
{
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *x0_tmp = N_VGetArrayPointer(x0);
  x0_tmp[0] = 1.0;
  x0_tmp[1] = 1.0;
  x0_tmp[3] = 1.0;
  x0_tmp[5] = 1.0;
  x0_tmp[7] = 1.0;
  x0_tmp[9] = 1.0;

  return;
}


 int dfxdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(long int N, realtype t, N_Vector x, 
  	N_Vector fx, DlsMat J, void *user_data, 
  	N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{
  int is;
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *dvdx = data->dvdx;
  dvdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(t, x, data);
  for (is=0; is<121; is++) {
    J->data[is] = 0.0;
  }
  J->data[1] = -dvdx[0];
  J->data[2] = dvdx[0];
  J->data[12] = -dvdx[10];
  J->data[13] = dvdx[10];
  J->data[23] = dvdx[21];
  J->data[24] = -dvdx[21];
  J->data[25] = -dvdx[22];
  J->data[26] = dvdx[22];
  J->data[36] = -dvdx[32];
  J->data[37] = dvdx[32];
  J->data[47] = dvdx[43];
  J->data[48] = -dvdx[43];
  J->data[49] = -dvdx[44];
  J->data[50] = dvdx[44];
  J->data[60] = -dvdx[54];
  J->data[61] = dvdx[54];
  J->data[71] = dvdx[65];
  J->data[72] = -dvdx[65];
  J->data[73] = -dvdx[66];
  J->data[74] = dvdx[66];
  J->data[75] = -dvdx[68];
  J->data[76] = dvdx[68];
  J->data[84] = -dvdx[76];
  J->data[85] = dvdx[76];
  J->data[93] = dvdx[85];
  J->data[94] = -dvdx[85];
  J->data[95] = dvdx[87];
  J->data[96] = -dvdx[87];
  J->data[108] = -dvdx[98];
  J->data[109] = dvdx[98];
  J->data[113] = -dvdx[102];
  J->data[114] = dvdx[102];
  J->data[119] = dvdx[109];
  J->data[120] = -dvdx[109];
  for (is=0; is<121; is++) {
    if(mxIsNaN(J->data[is])) J->data[is] = 0.0;
  }

  return(0);
}


 int fsx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(int Ns, realtype t, N_Vector x, N_Vector xdot, 
  	int ip, N_Vector sx, N_Vector sxdot, void *user_data, 
  	N_Vector tmp1, N_Vector tmp2)
{
  int is;
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *sv = data->sv;
  double *dvdx = data->dvdx;
  double *dvdu = data->dvdu;
  double *dvdp = data->dvdp;
  double *su = data->su;
  double *sx_tmp = N_VGetArrayPointer(sx);
  double *sxdot_tmp = N_VGetArrayPointer(sxdot);
  fsu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(data, t);
  dvdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(t, x, data);
  dvdu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(t, x, data);
  dvdp_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(t, x, data);
  for (is=0; is<10; is++) {
    sv[is] = 0.0;
  }
  sv[0] = dvdx[0]*sx_tmp[0]+dvdx[10]*sx_tmp[1];
  sv[1] = dvdx[21]*sx_tmp[2];
  sv[2] = dvdx[22]*sx_tmp[2]+dvdx[32]*sx_tmp[3]+dvdx[102]*sx_tmp[10];
  sv[3] = dvdx[43]*sx_tmp[4];
  sv[4] = dvdx[44]*sx_tmp[4]+dvdx[54]*sx_tmp[5];
  sv[5] = dvdx[65]*sx_tmp[6]+dvdx[85]*sx_tmp[8];
  sv[6] = dvdx[66]*sx_tmp[6]+dvdx[76]*sx_tmp[7];
  sv[7] = dvdx[87]*sx_tmp[8];
  sv[8] = dvdx[68]*sx_tmp[6]+dvdx[98]*sx_tmp[9];
  sv[9] = dvdx[109]*sx_tmp[10];
  switch (ip) {
    case 0: {
      sv[0] += dvdp[0];
    } break;
    case 1: {
      sv[1] += dvdp[11];
    } break;
    case 2: {
      sv[2] += dvdp[22];
    } break;
    case 3: {
      sv[3] += dvdp[33];
    } break;
    case 4: {
      sv[4] += dvdp[44];
    } break;
    case 5: {
      sv[6] += dvdp[56];
    } break;
    case 6: {
      sv[7] += dvdp[67];
    } break;
    case 7: {
      sv[8] += dvdp[78];
    } break;
    case 8: {
      sv[9] += dvdp[89];
    } break;
    case 9: {
      sv[5] += dvdp[95];
    } break;
  }
  sxdot_tmp[1] = -sv[0]+sv[1];
  sxdot_tmp[2] = sv[0]-sv[1];
  sxdot_tmp[3] = -sv[2]+sv[3];
  sxdot_tmp[4] = sv[2]-sv[3];
  sxdot_tmp[5] = -sv[4]+sv[5];
  sxdot_tmp[6] = sv[4]-sv[5];
  sxdot_tmp[7] = -sv[6]+sv[7];
  sxdot_tmp[8] = sv[6]-sv[7];
  sxdot_tmp[9] = -sv[8]+sv[9];
  sxdot_tmp[10] = sv[8]-sv[9];
  sxdot_tmp[0] = 0.0;
  for (is=0; is<11; is++) {
    if(mxIsNaN(sxdot_tmp[is])) sxdot_tmp[is] = 0.0;
  }

  return(0);
}


 void fsx0_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(int ip, N_Vector sx0, void *user_data)
{
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *sx0_tmp = N_VGetArrayPointer(sx0);
  switch (ip) {
  }

  return;
}


 void dfxdp_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, double *dfxdp, void *user_data)
{
  int is;
  UserData data = (UserData) user_data;
  double *p = data->p;
  double *u = data->u;
  double *dvdp = data->dvdp;
  double *dvdx = data->dvdx;
  double *dvdu = data->dvdu;
  double *x_tmp = N_VGetArrayPointer(x);
  dfxdp[1] = -dvdp[0];
  dfxdp[2] = dvdp[0];
  dfxdp[12] = dvdp[11];
  dfxdp[13] = -dvdp[11];
  dfxdp[25] = -dvdp[22];
  dfxdp[26] = dvdp[22];
  dfxdp[36] = dvdp[33];
  dfxdp[37] = -dvdp[33];
  dfxdp[49] = -dvdp[44];
  dfxdp[50] = dvdp[44];
  dfxdp[62] = -dvdp[56];
  dfxdp[63] = dvdp[56];
  dfxdp[73] = dvdp[67];
  dfxdp[74] = -dvdp[67];
  dfxdp[86] = -dvdp[78];
  dfxdp[87] = dvdp[78];
  dfxdp[97] = dvdp[89];
  dfxdp[98] = -dvdp[89];
  dfxdp[104] = dvdp[95];
  dfxdp[105] = -dvdp[95];
  for (is=0; is<110; is++) {
    if(mxIsNaN(dfxdp[is])) dfxdp[is] = 0.0;
  }

  return;
}


 void fz_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(double t, int nt, int it, int nz, int nx, int iruns, double *z, double *p, double *u, double *x){

  return;
}


 void fsz_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(double t, int nt, int it, int np, double *sz, double *p, double *u, double *x, double *z, double *su, double *sx){

  return;
}


 void dfzdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(double t, int nt, int it, int nz, int nx, int iruns, double *dfzdxs, double *z, double *p, double *u, double *x){

  return;
}


