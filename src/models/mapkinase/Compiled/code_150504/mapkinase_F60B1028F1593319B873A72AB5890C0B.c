#include "mapkinase_F60B1028F1593319B873A72AB5890C0B.h"
#include <cvodes/cvodes.h>
#include <cvodes/cvodes_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>
#include <udata.h>
#include <math.h>
#include <mex.h>
#include <arInputFunctionsC.h>



 void fy_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, int ny, int nx, int nz, int iruns, double *y, double *p, double *u, double *x, double *z){
  y[ny*nt*iruns+it+nt*0] = x[nx*ntlink*iruns+itlink+ntlink*10];
  y[ny*nt*iruns+it+nt*1] = x[nx*ntlink*iruns+itlink+ntlink*8];
  y[ny*nt*iruns+it+nt*2] = x[nx*ntlink*iruns+itlink+ntlink*6];
  y[ny*nt*iruns+it+nt*3] = x[nx*ntlink*iruns+itlink+ntlink*4];

  return;
}


 void fystd_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, double *ystd, double *y, double *p, double *u, double *x, double *z){
  ystd[it+nt*0] = 1.0E-1;
  ystd[it+nt*1] = 1.0E-1;
  ystd[it+nt*2] = 1.0E-1;
  ystd[it+nt*3] = 1.0E-1;

  return;
}


 void fsy_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, double *sy, double *p, double *u, double *x, double *z, double *su, double *sx, double *sz){
  sy[it+nt*0] = sx[itlink+ntlink*10];
  sy[it+nt*1] = sx[itlink+ntlink*8];
  sy[it+nt*2] = sx[itlink+ntlink*6];
  sy[it+nt*3] = sx[itlink+ntlink*4];
  sy[it+nt*4] = sx[itlink+ntlink*21];
  sy[it+nt*5] = sx[itlink+ntlink*19];
  sy[it+nt*6] = sx[itlink+ntlink*17];
  sy[it+nt*7] = sx[itlink+ntlink*15];
  sy[it+nt*8] = sx[itlink+ntlink*32];
  sy[it+nt*9] = sx[itlink+ntlink*30];
  sy[it+nt*10] = sx[itlink+ntlink*28];
  sy[it+nt*11] = sx[itlink+ntlink*26];
  sy[it+nt*12] = sx[itlink+ntlink*43];
  sy[it+nt*13] = sx[itlink+ntlink*41];
  sy[it+nt*14] = sx[itlink+ntlink*39];
  sy[it+nt*15] = sx[itlink+ntlink*37];
  sy[it+nt*16] = sx[itlink+ntlink*54];
  sy[it+nt*17] = sx[itlink+ntlink*52];
  sy[it+nt*18] = sx[itlink+ntlink*50];
  sy[it+nt*19] = sx[itlink+ntlink*48];
  sy[it+nt*20] = sx[itlink+ntlink*65];
  sy[it+nt*21] = sx[itlink+ntlink*63];
  sy[it+nt*22] = sx[itlink+ntlink*61];
  sy[it+nt*23] = sx[itlink+ntlink*59];
  sy[it+nt*24] = sx[itlink+ntlink*76];
  sy[it+nt*25] = sx[itlink+ntlink*74];
  sy[it+nt*26] = sx[itlink+ntlink*72];
  sy[it+nt*27] = sx[itlink+ntlink*70];
  sy[it+nt*28] = sx[itlink+ntlink*87];
  sy[it+nt*29] = sx[itlink+ntlink*85];
  sy[it+nt*30] = sx[itlink+ntlink*83];
  sy[it+nt*31] = sx[itlink+ntlink*81];
  sy[it+nt*32] = sx[itlink+ntlink*98];
  sy[it+nt*33] = sx[itlink+ntlink*96];
  sy[it+nt*34] = sx[itlink+ntlink*94];
  sy[it+nt*35] = sx[itlink+ntlink*92];
  sy[it+nt*36] = sx[itlink+ntlink*109];
  sy[it+nt*37] = sx[itlink+ntlink*107];
  sy[it+nt*38] = sx[itlink+ntlink*105];
  sy[it+nt*39] = sx[itlink+ntlink*103];

  return;
}


 void fsystd_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, double *systd, double *p, double *y, double *u, double *x, double *z, double *sy, double *su, double *sx, double *sz){


  return;
}


 void fy_scale_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, int ny, int nx, int nz, int iruns, double *y_scale, double *p, double *u, double *x, double *z, double *dfzdx){
  y_scale[ny*nt*iruns+it+nt*19] = 1.0;
  y_scale[ny*nt*iruns+it+nt*26] = 1.0;
  y_scale[ny*nt*iruns+it+nt*33] = 1.0;
  y_scale[ny*nt*iruns+it+nt*40] = 1.0;

  return;
}


