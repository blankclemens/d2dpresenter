#ifndef _MY_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686
#define _MY_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686

#include <cvodes/cvodes.h>
#include <cvodes/cvodes_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>
#include <udata.h>
#include <math.h>
#include <mex.h>
#include <arInputFunctionsC.h>



 void fu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(void *user_data, double t);
 void fsu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(void *user_data, double t);
 void fv_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data);
 void dvdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data);
 void dvdu_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data);
 void dvdp_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, void *user_data);
 int fx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, N_Vector xdot, void *user_data);
 void fxdouble_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, double *xdot_tmp, void *user_data);
 void fx0_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(N_Vector x0, void *user_data);
 int dfxdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(long int N, realtype t, N_Vector x,N_Vector fx, DlsMat J, void *user_data,N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);
 int fsx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(int Ns, realtype t, N_Vector x, N_Vector xdot,int ip, N_Vector sx, N_Vector sxdot, void *user_data,N_Vector tmp1, N_Vector tmp2);
 void fsx0_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(int ip, N_Vector sx0, void *user_data);
 void dfxdp_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(realtype t, N_Vector x, double *dfxdp, void *user_data);

 void fz_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(double t, int nt, int it, int nz, int nx, int iruns, double *z, double *p, double *u, double *x);
 void fsz_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(double t, int nt, int it, int np, double *sz, double *p, double *u, double *x, double *z, double *su, double *sx);

 void dfzdx_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686(double t, int nt, int it, int nz, int nx, int iruns, double *dfzdxs, double *z, double *p, double *u, double *x);
#endif /* _MY_mapkinase_BDE0505B5FE72F7B8CC36B2952CBC686 */



