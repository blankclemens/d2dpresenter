#ifndef _MY_mapkinase_F60B1028F1593319B873A72AB5890C0B
#define _MY_mapkinase_F60B1028F1593319B873A72AB5890C0B

#include <cvodes/cvodes.h>
#include <cvodes/cvodes_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>
#include <udata.h>
#include <math.h>
#include <mex.h>
#include <arInputFunctionsC.h>



 void fy_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, int ny, int nx, int nz, int iruns, double *y, double *p, double *u, double *x, double *z);
 void fystd_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, double *ystd, double *y, double *p, double *u, double *x, double *z);
 void fsy_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, double *sy, double *p, double *u, double *x, double *z, double *su, double *sx, double *sz);
 void fsystd_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, double *systd, double *p, double *y, double *u, double *x, double *z, double *sy, double *su, double *sx, double *sz);

 void fy_scale_mapkinase_F60B1028F1593319B873A72AB5890C0B(double t, int nt, int it, int ntlink, int itlink, int ny, int nx, int nz, int iruns, double *y_scale, double *p, double *u, double *x, double *z, double *dfzdx);
#endif /* _MY_mapkinase_F60B1028F1593319B873A72AB5890C0B */



